#include <stdio.h>
#include <stdlib.h>
#include <plist/plist++.h>
#include <mbedtls/base64.h>
#include <memory>
#include <sstream>
#include <map>
#include <boost/any.hpp>
#include <time.h>
#include "dp/keybag.h"
#include "dp/utils.h"

int main(int argc, char *argv[]) {

    auto keybag = BackupKeyBag::FromByteSequence(ExtractKeyBagFromPlist(argv[1]));
    keybag->DumpToFp(stdout);

    return 0;
}

