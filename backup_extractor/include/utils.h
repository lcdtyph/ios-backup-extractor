#ifndef __UTILS_H__
#define __UTILS_H__
#include <vector>
#include <string>
#include <stdint.h>

std::vector<uint8_t> ReadFileContent(const std::string &path);

#endif // __UTILS_H__

