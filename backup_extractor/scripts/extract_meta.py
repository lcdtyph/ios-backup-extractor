#!/usr/bin/env python3

import os
import sys
import mbfile_pb2
import mbfile_decoder
import sqlite3
import argparse
import struct

def ExtractMetaFromManifestDB(db_path, domains):
    query_cmd = 'select fileID, domain, relativePath, flags, file from Files'
    if not domains:
        domains = []
    for i, domain in enumerate(domains):
        if i == 0:
            query_cmd += ' where 1 = 0'
        query_cmd += " OR domain='%s'" % domain
    query_cmd += ' ORDER BY domain'
    dbconn = sqlite3.connect(db_path)

    dbconn.row_factory = sqlite3.Row
    cursor = dbconn.cursor()
    result = []
    for record in cursor.execute(query_cmd):
        source_file = record[0]
        domain      = record[1]
        rpath       = record[2]
        flags       = record[3]
        file_blob   = record[4]
        if flags == 0x4:
            continue
        mbfile = mbfile_decoder.UnarchiveWithKey(file_blob)
        param = mbfile_pb2.MBFileDecryptParameter()
        param.source_file   = os.path.join(source_file[:2], source_file)
        param.relative_path = os.path.join(domain, rpath);
        param.file_size     = mbfile.size
        param.file_mode     = mbfile.mode
        if mbfile.digest:
            param.digest    = mbfile.digest
        if mbfile.encryption_key:
            param.key       = mbfile.encryption_key
        if mbfile.target:
            param.target    = mbfile.target
        result.append(param)
    return result

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--database', default='Manifest.db-decrypted')
    parser.add_argument('-o', '--output', required=True)
    parser.add_argument('--domain', nargs='*')
    opt = parser.parse_args()

    metas = ExtractMetaFromManifestDB(opt.database, opt.domain)
    with open(opt.output, 'wb') as ofs:
        for meta in metas:
            meta_bytes = meta.SerializeToString()
            meta_len = struct.pack('>I', len(meta_bytes))
            ofs.write(meta_len)
            ofs.write(meta_bytes)
    print("%d metas dumped" % len(metas))

if __name__ == '__main__':
    main()

