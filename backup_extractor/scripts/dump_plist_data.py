#!/usr/bin/env python3

import plistlib
import argparse
import os

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--manifest', required=True)
    opt = parser.parse_args()

    manifest_plist = opt.manifest
    if not os.path.exists(manifest_plist):
        print("Invalid Manifest.plist path")
        exit()

    with open(manifest_plist, 'rb') as f:
        plist = plistlib.load(f)

    if not plist["IsEncrypted"]:
        print("Plain backup found, won't dump data")
        exit()
    with open('keybag.bin', 'wb') as ofs:
        ofs.write(bytes(plist["BackupKeyBag"]))
    with open('manifest_key.bin', 'wb') as ofs:
        ofs.write(bytes(plist["ManifestKey"]))
    print("Done")

if __name__ == '__main__':
    main()

