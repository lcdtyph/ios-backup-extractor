#!/usr/bin/env python3

from Foundation import *
import objc

class MBFile(NSObject):
    def new(self):
        self = super(MBFile, self).new()
        if self is None:
            return None
        else:
            self.__init__()
        return self

    def __init__(self):
        self.last_modified = 0
        self.protection_class = 0
        self.flags = 0
        self.group_id = 0
        self.last_status_change = 0
        self.relative_path = ''
        self.birth = 0
        self.encryption_key = b''
        self.size = 0
        self.mode = 0
        self.user_id = 0
        self.digest = b''
        self.inode_number = 0
        self.target = ''

    def initWithCoder_(self, aCoder):
        self.last_modified = aCoder.decodeIntegerForKey_('LastModified')
        self.protection_class = aCoder.decodeIntegerForKey_('ProtectionClass')
        self.flags = aCoder.decodeIntegerForKey_('Flags')
        self.group_id = aCoder.decodeIntegerForKey_('GroupID')
        self.last_status_change = aCoder.decodeIntegerForKey_('LastStatusChange')
        self.relative_path = str(aCoder.decodeObjectForKey_('RelativePath'))
        self.birth = aCoder.decodeIntegerForKey_('Birth')
        self.encryption_key = aCoder.decodeObjectForKey_('EncryptionKey')
        if self.encryption_key:
            self.encryption_key = bytes(self.encryption_key)
        self.size = aCoder.decodeIntegerForKey_('Size')
        self.mode = aCoder.decodeIntegerForKey_('Mode')
        self.user_id = aCoder.decodeIntegerForKey_('UserID')
        self.digest = aCoder.decodeObjectForKey_('Digest')
        if self.digest:
            self.digest = bytes(self.digest)
        self.inode_number = aCoder.decodeIntegerForKey_('InodeNumber')
        self.target = aCoder.decodeObjectForKey_('Target')
        if self.target:
            self.target = str(self.target)
        return self

def UnarchiveWithKey(data, key = 'root'):
    u = NSKeyedUnarchiver.alloc().initForReadingWithData_(data)
    return u.decodeObjectForKey_(key)

def lalala():
    data = open('/Users/lcdtyph/Desktop/sms.file_blob', 'rb').read()
    return UnarchiveWithKey(data)

if __name__ == '__main__':
    lalala()

