#include <fstream>

#include "utils.h"

std::vector<uint8_t> ReadFileContent(const std::string &path) {
    std::vector<uint8_t> result;
    std::ifstream ifs{path, std::ios::binary};
    if (!ifs) return result;
    ifs.seekg(0, std::ios::end);
    result.resize(ifs.tellg());
    ifs.seekg(0, std::ios::beg);
    ifs.read((char *)result.data(), result.size());
    return result;
}

