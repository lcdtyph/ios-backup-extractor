#include <vector>
#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/endian/buffers.hpp>
#include <gflags/gflags.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <dp/keybag.h>

#include "utils.h"

namespace bfs = boost::filesystem;

DEFINE_string(keybag, "keybag.bin", "Locked keybag");
DEFINE_string(password, "", "Password to unlock keybag");
DEFINE_string(manifest_key, "manifest_key.bin", "ManifestKey file");
DEFINE_string(encrypted_database, "", "Encrypted Manifest.db path");

DEFINE_string(database, "Manifest.db-decrypted", "Decrypted Manifest.db path");
DEFINE_string(output_keybag, "unlocked_keybag.bin", "Unlocked keybag");

static bool CheckFlags(const char *flagname, const std::string &value);

DEFINE_validator(keybag, &CheckFlags);
DEFINE_validator(password, &CheckFlags);
DEFINE_validator(encrypted_database, &CheckFlags);
DEFINE_validator(manifest_key, &CheckFlags);
DEFINE_validator(database, &CheckFlags);
DEFINE_validator(output_keybag, &CheckFlags);

static bool GetManifestKey(
    const std::string &key_path,
    std::shared_ptr<BackupKeyBag> keybag,
    std::vector<uint8_t> &manifest_key
);

int main(int argc, char *argv[]) {
    google::ParseCommandLineFlags(&argc, &argv, true);

    auto keybag = BackupKeyBag::FromByteSequence(ReadFileContent(FLAGS_keybag));
    if (!keybag) {
        std::cerr << "Invalid keybag" << std::endl;
        std::exit(-1);
    }

    std::cout << "Unlocking keybag..." << std::endl;
    if (!keybag->UnlockWithPasscode(FLAGS_password)) {
        std::cerr << "Incorrect password" << std::endl;
        std::exit(-1);
    }
    {
        std::ofstream ofs{FLAGS_output_keybag, std::ios::binary};
        if (!ofs) {
            std::cerr << "Failed to dump unlocked keybag" << std::endl;
            std::exit(-1);
        }
        auto unlocked_keybag = keybag->DumpUnlockedKeybag();
        ofs.write((const char *)unlocked_keybag.data(), unlocked_keybag.size());
        ofs.close();
    }
    std::cout << "Unlocked. Decrypting Manifest.db..." << std::endl;
    std::vector<uint8_t> manifest_key;
    std::array<uint8_t, 16> iv;
    std::fill(iv.begin(), iv.end(), 0);
    if (!GetManifestKey(FLAGS_manifest_key, keybag, manifest_key)) {
        std::cerr << "Invalid ManifestKey" << std::endl;
        std::exit(-1);
    }

    {
        std::ifstream ifs{FLAGS_encrypted_database, std::ios::binary};
        std::ofstream ofs{FLAGS_database, std::ios::binary};
        std::array<uint8_t, 8192> in_buf;
        std::array<uint8_t, 8192 + 32> out_buf;
        int len;
        if (!ifs) {
            std::cerr << "Cannot open " << FLAGS_encrypted_database << std::endl;
            std::exit(-1);
        }
        if (!ofs) {
            std::cerr << "Cannot open " << FLAGS_database << std::endl;
            std::exit(-1);
        }
        auto ctx = EVP_CIPHER_CTX_new();
        EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), nullptr, manifest_key.data(), iv.data());
        while (true) {
            ifs.read((char *)in_buf.data(), in_buf.size());
            int ilen = (int)ifs.gcount();
            if (ilen == 0) {
                break;
            }
            len = (int)out_buf.size();
            if (EVP_DecryptUpdate(ctx, out_buf.data(), &len, in_buf.data(), ilen) != 1) {
                std::cerr << "OpenSSL error: " << std::endl;
                ERR_print_errors_fp(stderr);
                std::exit(-1);
            }
            ofs.write((const char *)out_buf.data(), len);
        }
        len = (int)out_buf.size();
        EVP_DecryptFinal_ex(ctx, out_buf.data(), &len);
        if (len) {
            ofs.write((const char *)out_buf.data(), len);
        }
        EVP_CIPHER_CTX_free(ctx);
    }
    std::cout << "Manifest.db decrypted" << std::endl;

    google::ShutDownCommandLineFlags();
    return 0;
}

static bool CheckFlags(const char *flagname, const std::string &value) {
    if (value.empty()) {
        std::cerr << "Invalid parameter: " << flagname << std::endl;
        return false;
    }
    if (strcmp(flagname, "keybag") == 0 ||
        strcmp(flagname, "manifest_key") == 0 ||
        strcmp(flagname, "encrypted_database") == 0) {
        if (!bfs::exists(value)) {
            std::cerr << "Invalid path of " << flagname << std::endl;
            return false;
        }
    }
    return true;
}

static bool GetManifestKey(
    const std::string &key_path,
    std::shared_ptr<BackupKeyBag> keybag,
    std::vector<uint8_t> &manifest_key
) {
    auto key_data = ReadFileContent(key_path);
    if (key_data.size() < 44) return false;
    boost::endian::little_uint32_buf_t clas;
    memcpy(&clas, key_data.data(), sizeof clas);
    key_data.erase(key_data.begin(), key_data.begin() + sizeof clas);
    std::cout << "ManifestKey class: " << clas.value() << std::endl;
    std::cout << "ManifestKey length: " << key_data.size() << std::endl;
    return keybag->UnwrapKeyOfClass(clas.value(), key_data, manifest_key);
}
