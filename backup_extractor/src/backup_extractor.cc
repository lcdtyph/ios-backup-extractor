#include <fstream>
#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/endian/buffers.hpp>
#include <openssl/evp.h>
#include <gflags/gflags.h>
#include <glog/logging.h>
#include <dp/keybag.h>

#include "thread_pool.h"
#include "utils.h"
#include "mbfile.pb.h"

namespace bfs = boost::filesystem;

DEFINE_string(keybag, "unlocked_keybag.bin", "Unlocked Backup Keybag data file");
DEFINE_string(backup_path, "", "Backup path");
DEFINE_string(meta_file, "", "Metas of extract file");
DEFINE_string(output, "", "Output directory");
DEFINE_uint32(batch, 1, "Batch size");

static bool CheckFlags(const char *flagname, const std::string &value);

DEFINE_validator(keybag, &CheckFlags);
DEFINE_validator(backup_path, &CheckFlags);
DEFINE_validator(meta_file, &CheckFlags);
DEFINE_validator(output, &CheckFlags);

static void ProcessFileMeta(
    std::shared_ptr<BackupKeyBag> keybag,
    const std::string &backup_path,
    const std::string &output_path,
    backup_extractor::MBFileDecryptParameter meta,
    std::ios::off_type offset
);

int main(int argc, char *argv[]) {
    google::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);

    FLAGS_batch = std::max(FLAGS_batch, (uint32_t)1);
    auto keybag = BackupKeyBag::UnlockedFromBytes(ReadFileContent(FLAGS_keybag));
    if (!keybag || !keybag->unlocked()) {
        LOG(FATAL) << "Invalid keybag";
        std::exit(-1);
    }

    if (!bfs::exists(FLAGS_output)) {
        if (!bfs::create_directories(FLAGS_output)) {
            LOG(FATAL) << "Cannot create output directory";
        }
    }

    std::ifstream ifs{FLAGS_meta_file, std::ios::binary};
    std::vector<uint8_t> buf;
    ThreadPool pool(FLAGS_batch);
    std::vector<std::future<void>> results;
    while (ifs) {
        boost::endian::big_uint32_buf_t meta_len;
        ifs.read((char *)&meta_len, sizeof meta_len);
        if (ifs.gcount() == 0) {
            LOG(INFO) << "Extract meta completed";
            break;
        } else if (ifs.gcount() != sizeof meta_len) {
            LOG(ERROR) << "Incompleted metafile, process terminate";
            break;
        }

        buf.resize(meta_len.value());
        ifs.read((char *)buf.data(), buf.size());
        if (ifs.gcount() != buf.size()) {
            LOG(ERROR) << "Incompleted metafile, process terminate";
            break;
        }
        backup_extractor::MBFileDecryptParameter file_meta;
        if (!file_meta.ParseFromArray(buf.data(), buf.size())) {
            LOG(WARNING) << "Invalid record at offset "
                         << ((size_t)ifs.tellg() - buf.size()) << ", skipped";
            continue;
        }
        results.emplace_back(
            std::move(pool.PushTask(&ProcessFileMeta,
                keybag, std::cref(FLAGS_backup_path),
                std::cref(FLAGS_output), std::move(file_meta),
                (size_t)ifs.tellg() - buf.size()
            ))
        );
    }

    for (auto &result : results) {
        result.get();
    }
    LOG(INFO) << results.size() << " items proccessed.";

    google::ShutdownGoogleLogging();
    google::ShutDownCommandLineFlags();
    return 0;
}

static bool CheckFlags(const char *flagname, const std::string &value) {
    if (value.empty()) {
        std::cerr << "Invalid parameter: " << flagname << std::endl;
        return false;
    }
    if (strcmp(flagname, "meta_file") == 0 ||
        strcmp(flagname, "keybag") == 0 ||
        strcmp(flagname, "backup_path") == 0) {
        if (!bfs::exists(value)) {
            std::cerr << "Invalid path of " << flagname << std::endl;
            return false;
        }
    }
    return true;
}

static void ProcessFileMeta(
    std::shared_ptr<BackupKeyBag> keybag,
    const std::string &backup_path,
    const std::string &output_path,
    backup_extractor::MBFileDecryptParameter meta,
    std::ios::off_type offset
) {
    bfs::path dest_root_path{output_path};
    bfs::path src_root_path{backup_path};
    boost::endian::little_uint32_buf_t clas;
    std::vector<uint8_t> file_enc_key;
    if (meta.has_key()) {
        auto key_str = meta.key();
        key_str.copy((char *)&clas, 4);
        std::vector<uint8_t> wrapped_key{key_str.begin() + 4, key_str.end()};
        if (!keybag->UnwrapKeyOfClass(clas.value(), wrapped_key, file_enc_key)) {
            LOG(WARNING) << "Failed to unwrap key, record at offset: "
                         << offset << ", skipped";
            return;
        }
    }

    uint32_t file_type = meta.file_mode() & 0xf000;
    auto dest_full_path = dest_root_path / meta.relative_path();
    auto src_full_path  = src_root_path / meta.source_file();
    LOG(INFO) << "Start to process " << src_full_path;
    boost::system::error_code ec;
    if (file_type == 0x4000) { // directory
        if (!bfs::exists(dest_full_path)) {
            if (!bfs::create_directories(dest_full_path, ec)) {
                LOG(WARNING) << "Failed to create directories: " << dest_full_path
                             << ", with error: " << ec.message();
            }
        }
        LOG(INFO) << "Created directory " << dest_full_path;
        return;
    } else if ((file_type == 0x8000) || (file_type == 0xa000)) { // regular file or symbolic link
        if (file_type == 0xa000) {
            if (!meta.has_target()) {
                LOG(ERROR) << "Got a symbolic link without a target, skip " << dest_full_path;
                return;
            }
            dest_full_path = dest_root_path / meta.target();
            LOG(INFO) << "Destination reset to " << dest_full_path;
        }
        LOG(INFO) << "Copying " << src_full_path << " to " << dest_full_path;
        auto dest_dir = dest_full_path.parent_path();
        if (!bfs::exists(dest_dir)) {
            if (!bfs::create_directories(dest_dir, ec)) {
                LOG(WARNING) << "Failed to create directories: " << dest_dir
                             << ", with error: " << ec.message();
            }
        }
        if (file_enc_key.empty()) {
            bfs::copy(src_full_path, dest_full_path, ec);
            if(ec) {
                LOG(WARNING) << "Failed to copy " << src_full_path
                             << ", with error: " << ec.message();
            }
        } else {
            LOG(INFO) << "Decrypting... " << src_full_path;
            std::ifstream ifs{src_full_path.string(), std::ios::binary};
            std::ofstream ofs{dest_full_path.string(), std::ios::binary};
            if (!ifs) {
                LOG(ERROR) << "Cannot open " << src_full_path;
                return;
            }
            if (!ofs) {
                LOG(ERROR) << "Cannot open " << dest_full_path;
                return;
            }
            std::array<uint8_t, 8192> in_buf;
            std::array<uint8_t, 8192 + 32> out_buf;
            std::array<uint8_t, 16> iv;
            std::fill(iv.begin(), iv.end(), 0);
            auto ctx = EVP_CIPHER_CTX_new();
            EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), nullptr, file_enc_key.data(), iv.data());
            int len;
            while (true) {
                ifs.read((char *)in_buf.data(), in_buf.size());
                auto in_len = ifs.gcount();
                if (in_len == 0) break;
                len = (int)out_buf.size();
                if (EVP_DecryptUpdate(ctx, out_buf.data(), &len, in_buf.data(), (int)in_len) != 1) {
                    LOG(ERROR) << "OpenSSL Decrypt error, skip file " << src_full_path;
                    return;
                }
                ofs.write((const char *)out_buf.data(), len);
            }
            len = (int)out_buf.size();
            EVP_DecryptFinal_ex(ctx, out_buf.data(), &len);
            if (len) {
                ofs.write((const char *)out_buf.data(), len);
            }
            EVP_CIPHER_CTX_free(ctx);
            LOG(INFO) << "Decrypted " << ofs.tellp()
                      << " bytes, designed size: " << meta.file_size();
        }
    }
    LOG(INFO) << "Completed extract " << src_full_path
              << " to " << dest_full_path;
}

