
#ifndef __COMM_H__
#define __COMM_H__

#include <utility>
#include <functional>
#include <boost/asio.hpp>
#include <boost/endian/buffers.hpp>

#include "dp/keybag.h"
#include "worker.pb.h"

using boost::asio::ip::tcp;
class Session : public std::enable_shared_from_this<Session> {
public:
    using CallBack = std::function<ReplyMessage::AckMessage(CommandMessage, std::shared_ptr<Session>)>;

    Session(tcp::socket socket, CallBack cb)
        :socket_(std::move(socket)), cb_(std::move(cb)) {
    }

    void start() {
        doReadCommandHeader();
    }

    void sendResult(ReplyMessage::TaskResult result) {
        doWriteResult(std::move(result));
    }

    boost::asio::io_context &getContext() {
        return socket_.get_executor().context();
    }

private:
    void doReadCommandHeader() {
        auto self(shared_from_this());
        boost::asio::async_read(socket_, boost::asio::buffer(&len_, sizeof len_),
            boost::asio::transfer_exactly(sizeof len_),
            [this, self](boost::system::error_code ec, std::size_t length) {
                if (ec || length != sizeof len_) {
                    fprintf(stderr, "ec: %s\n", ec.message().c_str());
                    return;
                }
                
                doReadCommand();
            });
    }

    void doReadCommand() {
        auto self(shared_from_this());
        buf_.resize(len_.value());
        boost::asio::async_read(socket_, boost::asio::buffer(buf_),
            boost::asio::transfer_at_least(buf_.size()),
            [this, self](boost::system::error_code ec, std::size_t length) {
                if (ec || length != len_.value()) {
                    fprintf(stderr, "ec: %s\n", ec.message().c_str());
                    return;
                }
                CommandMessage cmd;
                if (!cmd.ParseFromString(buf_)) {
                    fprintf(stderr, "protobuf parse error!\n");
                    return;
                }
                handleCommand(std::move(cmd));
            });
    }

    void doWriteAck(ReplyMessage::AckMessage ack) {
        auto self(shared_from_this());
        ReplyMessage reply;
        *reply.mutable_ack() = ack;
        reply.SerializeToString(&buf_);
        len_ = buf_.size();
        boost::asio::async_write(socket_,
            std::array<boost::asio::const_buffer, 2>{ boost::asio::buffer(&len_, sizeof len_),
                                                      boost::asio::buffer(buf_) },
            [this, self](boost::system::error_code ec, std::size_t length) {
                if (!ec) {
                    doReadCommandHeader();
                }
            });
    }

    void doWriteResult(ReplyMessage::TaskResult result) {
        auto self(shared_from_this());
        ReplyMessage reply;
        *reply.mutable_result() = result;
        reply.SerializeToString(&resultBuf_);
        resultLen_ = resultBuf_.size();
        boost::asio::async_write(socket_,
            std::array<boost::asio::const_buffer, 2>{ boost::asio::buffer(&resultLen_, sizeof resultLen_),
                                                      boost::asio::buffer(resultBuf_) },
            [this, self](boost::system::error_code ec, std::size_t length) {
                if (ec) {
                    fprintf(stderr, "write result failed!!\n");
                    for (auto &c : resultBuf_) {
                        fprintf(stderr, "%02hhx", c);
                    }
                    fputs("\n", stderr);
                }
            });
    }

    void handleCommand(CommandMessage cmd) {
        auto self(shared_from_this());
        auto reply = cb_(cmd, self);
        doWriteAck(reply);
    }

    CallBack cb_;
    tcp::socket socket_;
    std::string buf_;
    boost::endian::big_uint32_buf_t len_;
    std::string resultBuf_;
    boost::endian::big_uint32_buf_t resultLen_;
};

class Server {
public:
    using CallBack = Session::CallBack;
    Server(boost::asio::io_context &ctx, uint16_t port, CallBack cb)
        :acceptor_(ctx, tcp::endpoint(tcp::v4(), port)), cb_(std::move(cb)) {
        doAccept();
    }

private:
    void doAccept() {
        acceptor_.async_accept([this](boost::system::error_code ec, tcp::socket socket) {
            if (!ec) {
                std::make_shared<Session>(std::move(socket), cb_)->start();
            }
            doAccept();
        });
    }

    tcp::acceptor acceptor_;
    CallBack cb_;
};

class Client : public std::enable_shared_from_this<Client> {
public:
    using CallBack = std::function<bool(const std::string &,
                                        std::shared_ptr<Client>,
                                        CommandMessage &,
                                        bool &)>;

    Client(boost::asio::io_context &ctx, std::string host,
           std::string port, CommandMessage::InitialMessage init, CallBack cb)
        :resolver_(ctx), socket_(ctx),
         init_(std::move(init)), cb_(cb),
         host_(std::move(host)), port_(std::move(port)) {

    }

    void start() {
        doResolve();
    }

    boost::asio::io_context &getContext() {
        return socket_.get_executor().context();
    }

private:
    void doResolve() {
        auto self(shared_from_this());
        resolver_.async_resolve(host_, port_,
        [this, self](const boost::system::error_code& ec, tcp::resolver::iterator itr) {
            if (ec) {
                fprintf(stderr, "Resolve error: %s\n", ec.message().c_str());
                return;
            }
            doConnect(itr);
        });
    }

    void doConnect(tcp::resolver::iterator itr) {
        auto self(shared_from_this());
        boost::asio::async_connect(socket_, itr,
        [this, self](boost::system::error_code ec, tcp::resolver::iterator itr) {
            if (itr == tcp::resolver::iterator()) {
                fprintf(stderr, "Cannot connect\n");
                return;
            }
            fprintf(stdout, "Successfully connected to %s\n", itr->host_name().c_str());
            doWriteInitMsg();
        });
    }

    void doWriteInitMsg() {
        auto self(shared_from_this());
        CommandMessage cmd;
        *cmd.mutable_init() = init_;
        cmd.SerializeToString(&buf_);
        len_ = buf_.size();
        doWriteBuf();
    }

    void doWriteBuf() {
        auto self(shared_from_this());
        boost::asio::async_write(socket_,
            std::array<boost::asio::const_buffer, 2>{ boost::asio::buffer(&len_, sizeof len_),
                                                      boost::asio::buffer(buf_) },
            [this, self](boost::system::error_code ec, std::size_t length) {
                if (ec) {
                    fprintf(stderr, "write initmsg failed\n");
                    return;
                }
                doReadHeader();
            });
    }

    void doReadHeader() {
        auto self(shared_from_this());
        boost::asio::async_read(socket_, boost::asio::buffer(&len_, sizeof len_),
            boost::asio::transfer_exactly(sizeof len_),
            [this, self](boost::system::error_code ec, std::size_t length) {
                if (ec || length != sizeof len_) {
                    fprintf(stderr, "ec: %s\n", ec.message().c_str());
                    return;
                }
                
                doRead();
            });
    }

    void doRead() {
        auto self(shared_from_this());
        buf_.resize(len_.value());
        boost::asio::async_read(socket_, boost::asio::buffer(buf_),
            boost::asio::transfer_at_least(buf_.size()),
            [this, self](boost::system::error_code ec, std::size_t length) {
                if (ec || length != len_.value()) {
                    fprintf(stderr, "ec: %s\n", ec.message().c_str());
                    return;
                }
                handleReply();
            });
    }

    void handleReply() {
        auto self(shared_from_this());
        CommandMessage cmd;
        bool cont;
        bool shutdown = cb_(buf_, self, cmd, cont);

        if (shutdown) {
            return;
        } else if (!cont) {
            doReadHeader();
            return;
        }
        fprintf(stdout, "write task\n");
        cmd.SerializeToString(&buf_);
        len_ = buf_.size();
        doWriteBuf();
    }

    CallBack cb_;
    std::string host_;
    std::string port_;
    tcp::resolver resolver_;
    tcp::socket socket_;
    CommandMessage::InitialMessage init_;
    std::string buf_;
    boost::endian::big_uint32_buf_t len_;
};

#endif

