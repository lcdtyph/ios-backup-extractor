
#include "handler.h"

std::vector<std::string> Handler::kPaths;
decltype(Handler::kPaths)::iterator Handler::kCurr;
std::ifstream Handler::kIfs;

void Handler::Initialize(std::vector<std::string>&& paths) {
    kPaths = paths;
    kCurr = kPaths.begin();
}

bool Handler::getNextPasscode(std::string &buf) {
    buf.clear();

    if (!kIfs || !std::getline(kIfs, buf)) {
        while (kCurr != kPaths.end()) {
            kIfs.open(*kCurr);
            if (kIfs) break;
            ++kCurr;
        }
        if (!kIfs) return false;
        std::getline(kIfs, buf);
    }
    return true;
}

