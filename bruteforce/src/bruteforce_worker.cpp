#include <cstdio>
#include <utility>
#include <functional>
#include <boost/asio.hpp>
#include <boost/endian/buffers.hpp>
#include <gflags/gflags.h>

#include "dp/keybag.h"
#include "worker.pb.h"
#include "comm.h"

DEFINE_uint32(port, 58888, "port listening at");

ReplyMessage::AckMessage CommandCB(CommandMessage cmd,
                     std::shared_ptr<Session> session,
                     boost::asio::thread_pool &pool);

int main(int argc, char *argv[]) {
    try {
        boost::asio::thread_pool pool(1);

        boost::asio::io_context ctx;
        Server s(ctx, FLAGS_port,
                 std::bind(CommandCB,
                 std::placeholders::_1,
                 std::placeholders::_2,
                 std::ref(pool)));

        ctx.run();
    } catch (std::exception& e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}

ReplyMessage::AckMessage CommandCB(CommandMessage cmd,
                     std::shared_ptr<Session> session,
                     boost::asio::thread_pool &pool) {
    static std::shared_ptr<BackupKeyBag> keybag;
    ReplyMessage::AckMessage ack;

    if (cmd.has_init()) {
        CommandMessage::InitialMessage *initMsg = cmd.mutable_init();
        auto data = initMsg->keybag();
        auto newkeybag = BackupKeyBag::FromString(data);
        if (newkeybag) {
            fprintf(stdout, "Keybag updated:\n");
            newkeybag->DumpToFp(stdout);
            keybag = newkeybag;
            ack.set_success(true);
        }
    } else if (cmd.has_task()) {
        if (!keybag) {
            ack.set_success(false);
            ack.set_detail("uninitialized worker, tasks will be ignored.");
        } else {
            auto tasks = cmd.task();
            fprintf(stdout, "Got task\n");
            boost::asio::post(pool, [tasks = std::move(tasks), session, kb = keybag]() {
                ReplyMessage::TaskResult result;
                for (int i = 0; i < tasks.passcodes_size(); ++i) {
                    auto psc = tasks.passcodes(i);
                    fprintf(stdout, "trying: %s\n", psc.c_str());
                    if (kb->UnlockWithPasscode(psc)) {
                        result.set_passcode(psc);
                        result.set_found(true);
                        fprintf(stdout, "found\n");
                        break;
                    }
                }
                boost::asio::post(session->getContext(),
                    [session, result = std::move(result)]() {
                        session->sendResult(std::move(result));
                });
            });
            ack.set_success(true);
        }
    } else {
        fprintf(stdout, "unexcepted command\n");
    }
    return ack;
}

