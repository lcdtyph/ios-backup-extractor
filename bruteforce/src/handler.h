#ifndef __HANDLER_H__
#define __HANDLER_H__

#include <vector>
#include <string>
#include <utility>
#include <fstream>

#include "comm.h"
#include "worker.pb.h"

class Handler {
public:

    static void Initialize(std::vector<std::string>&& paths);

    bool operator()(const std::string &buf, std::shared_ptr<Client> c,
                    CommandMessage &cmd, bool &cont) {
        ReplyMessage reply;
        if (!reply.ParseFromString(buf)) {
            fprintf(stderr, "cannot parse reply\n");
            return true;
        }
        if (reply.has_ack()) {
            auto ack = reply.ack();
            fprintf(stdout, "Got Ack\n");
            if (!ack.success()) {
                fprintf(stderr, "error occured: %s\n", ack.detail().c_str());
                return true;
            }
            fillTask(cmd.mutable_task());
            cont = (cmd.mutable_task()->passcodes_size() != 0);
            return false;
        } else if (reply.has_result()) {
            auto result = reply.result();
            fprintf(stdout, "Got Result\n");
            if (result.found()) {
                c->getContext().stop();
                fprintf(stdout, "Found: %s\n", result.passcode().c_str());
                return true;
            }
            cont = false;
            return false;
        }
        return true;
    }

private:
    void fillTask(CommandMessage::TaskMessage *tasks) {
        std::string psc;
        for (int i = 0; i < 8 && getNextPasscode(psc); ++i) {
            tasks->add_passcodes(psc);
        }
    }

    static bool getNextPasscode(std::string &buf);

    static std::vector<std::string> kPaths;
    static decltype(kPaths)::iterator kCurr;
    static std::ifstream kIfs;
};

#endif

