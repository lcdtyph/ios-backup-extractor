#include <boost/process.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <string>
#include <vector>
#include <cstdio>
#include <fstream>
#include <algorithm>
#include <gflags/gflags.h>

#include "dp/utils.h"
#include "dp/keybag.h"
#include "comm.h"
#include "handler.h"
#include "worker.pb.h"

DEFINE_string(dict, "", "specific one passcode dictionary path");
DEFINE_string(dicts_dir, "", "specific passcode dictionaries directory");
DEFINE_string(backup, "", "iOS backup directory");
DEFINE_string(workers, "", "worker endpoints splitted by comma, each worker must be `host:port'");

bool PrepareArgs(std::vector<std::string> &dictPaths,
                 std::vector<std::string> &workers,
                 std::string &keybag);

int main(int argc, char *argv[]) {
    google::ParseCommandLineFlags(&argc, &argv, true);

    std::vector<std::string> paths, workers;
    std::string keybag;
    if (!PrepareArgs(paths, workers, keybag)) {
        fputs("invalid backup or invalid dictionaries!\n", stderr);
        return -1;
    }

    Handler::Initialize(std::move(paths));

    boost::asio::io_context ctx;

    CommandMessage::InitialMessage initMsg;
    initMsg.set_keybag(keybag);
    
    for (auto &ep : workers) {
        std::vector<std::string> bufs;
        boost::algorithm::split(bufs, ep, boost::is_any_of(":"));
        if (bufs.size() != 2) continue;
        std::make_shared<Client>(ctx, bufs[0], bufs[1], initMsg, Handler())->start();
    }

    ctx.run();

    return 0;
}

bool PrepareArgs(std::vector<std::string> &dictPaths,
                 std::vector<std::string> &workers,
                 std::string &keybag) {
    namespace bfs = boost::filesystem;
    auto manifestPath = bfs::path(FLAGS_backup) / "Manifest.plist";
    auto buf = ExtractKeyBagFromPlist(manifestPath.string());
    keybag = std::string{buf.begin(), buf.end()};
    if (keybag.empty()) {
        return false;
    }

    if (!FLAGS_dict.empty()) dictPaths.push_back(FLAGS_dict);
    if (bfs::exists(bfs::directory_entry(bfs::path(FLAGS_dicts_dir)))) {
        std::for_each(bfs::directory_iterator{FLAGS_dicts_dir}, bfs::directory_iterator{},
                     [&dictPaths](const bfs::directory_entry &de) {
                         if (bfs::is_regular_file(de.status())) {
                             dictPaths.push_back(de.path().string());
                         }
                     });
    }
    if (dictPaths.empty()) {
        return false;
    }

    boost::split(workers, FLAGS_workers, boost::is_any_of(","));
    if (workers.empty()) {
        return false;
    }
    return true;
}

